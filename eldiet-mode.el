;;; eldiet-mode.el --- Major mode for editing pyet journal files and running reports.

;; Copyright (C) 2020 Aaron Meinel

;; Author: Aaron Meinel <yourname@example.com>
;; Created: 22 Mar 2020
;; Keywords: diet health PIM nutrition
;; Homepage: https://gitlab.com/aaron_meinel/eldiet , https://gitlab.com/aaron_meinel/pyet

;; This file is not part of GNU Emacs.



;; along with this file.  If not, see <https://www.gnu.org/licenses/>.

;;; Code

(require 'generic-x)
(require 'f)

(defvar eldiet-data-file "/home/aaron/Programming/my_diet/eldiet/data.csv")
(defvar eldiet-report-command "pyet")
(defvar eldiet-mode-map 
  (let ((map (make-sparse-keymap))) 
    (define-key map (kbd "C-c C-c") 'eldiet-run-daily-report) 
    (define-key map (kbd "M-RET") 'eldiet-complete-from-db)
    (define-key map (kbd "RET") 'newline-and-indent) map))

(defvar eldiet-completion-table nil)






(defun eldiet-set-eldiet-completion-table () 
  (interactive)
  "Read the csv with food data. Since we enforce a strong
  structure for this, it is more efficient not to use any of the
  csv libraries but f directly instead." 
  (setq eldiet-completion-table (mapcar #'car (mapcar (lambda (line) 
                                                         (split-string line ",")) 
                                                       (split-string (f-read-text eldiet-data-file)
                                                                     "\n")))))

(defun eldiet-run-daily-report () 
  (interactive) 
  (shell-command (concat eldiet-report-command " " (buffer-name) " " eldiet-data-file)))


(defun eldiet-complete-from-db () 
  (interactive) 
  (helm :sources (helm-build-sync-source "Select food:" 
                   :candidates eldiet-completion-table 
                   :action (lambda (candidate) 
                             (insert candidate))) 
        :buffer "*Food Database*"))






(define-generic-mode 'eldiet-mode ;; name of the mode to create
  '("#") ;; comments start with # (not implemented)
  '("Breakfast" "Lunch" "Dinner" "Snack") ;; some keywords
  '();; no fontlock operators so far
  '("\\.diet$") ;; files for which to activate this mode
  '(eldiet-set-eldiet-completion-table
    (lambda () (use-local-map eldiet-mode-map)))
                                        ;    #'(use-local-map eldiet-mode-map)) ;; other functions to call

  "A mode for dietlog files" ;; doc string for this mode
)





(provide 'eldiet-mode)
;;; eldiet-mode.el ends here
